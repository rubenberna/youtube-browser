# Youtube

> A Vue.js project
It uses the component communication between parent and child of vue.js to fetch videos, display a list and play a selected video

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
